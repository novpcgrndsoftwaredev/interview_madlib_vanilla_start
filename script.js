//Firebase snapshot, this is helpful for part 2 of the challenge
firebase.database().ref().once("value").then(function(snapshot){
    console.log(snapshot.val());
})

var _stories, _nouns, _adjectives, _adverbs, _verbs;

var _words = {
    nouns: '',
    verbs: '',
    adjectives: '',
    adverbs: ''
};
var _storyName = '';
var _storyText = '';
var _currStory = '';
var _storyTypes = [];

_stories = getStoriesFromDb();
_storyTypes = getStories();
updateUIWithStories();

_adjectives = getAdjectives();
_adverbs = getAdverbs();
_nouns = getNouns();
_verbs = getVerbs();

function updateUIWithStories(){
    var parentEl = document.getElementById('storyTypes');
    var label, input;
    for(var i = 0; i < _storyTypes.length; i++){
        label = document.createElement("LABEL");
        label.innerText = _storyTypes[i];
        input = document.createElement("INPUT");
        input.setAttribute("type", "radio");
        input.setAttribute("onclick", "radioBtnClick(' " + _storyTypes[i] + " ')");
        label.appendChild(input);
        parentEl.appendChild(label);
    }
}

function radioBtnClick(storyType){
    _currStory = storyType;
}

function generateStory(){
    _storyName = _currStory;
    _words.nouns = document.getElementById('nouns').value;
    _words.adverbs = document.getElementById('adverbs').value;
    _words.verbs = document.getElementById('verbs').value;
    _words.adjectives = document.getElementById('adjectives').value;
    _storyText = createStory(_storyName, _words);
    document.getElementById('storyname').innerText = _storyName;
    document.getElementById('storyText').innerText = _storyText;
}

function injectWords(story,words){
    for(var key in words){
        if(words.hasOwnProperty(key)){
            for(var i = 0, target; i < words[key].length; i++){
                target = '#' + key;
                story = story.replace(target, words[key][i]);
            }
        }
    }
    return story;
}

function shuffle(arr) {
    var currIndex = arr.length, temp, randIndex ;
    // While there remain elements to shuffle...
    while (-1 !== currIndex) {
        // Pick a remaining element...
        randIndex = Math.floor(Math.random() * currIndex);
        currIndex -= 1;
        // And swap it with the current element.
        temp = arr[currIndex];
        arr[currIndex] = arr[randIndex];
        arr[randIndex] = temp;
    }
    return arr;
}
function combineWords(inputWords,defaultWords){
    var words = {
        nouns: new Array(4),
        verbs: new Array(4),
        adjectives: new Array(4),
        adverbs: new Array(4)
    };
    for(var key in defaultWords){
        if(defaultWords.hasOwnProperty(key)){
            shuffle(defaultWords[key]);
        }
    }
    for(var type in words){
        if(words.hasOwnProperty(type)){
            for (var i = 0; i < words[type].length; i++) {
                words[type][i] = (inputWords[type][i] !== undefined) ? inputWords[type][i] : defaultWords[type][i];
            }
        }
    }
    return words;
}

function stringObjToArrObj(obj){
    for(var key in obj){
        if(obj.hasOwnProperty(key) && typeof obj[key] !== 'object'){
            obj[key] =  obj[key].length > 0 ? obj[key].split(',') : [];
        }
    }
    return obj;
}

function createStory(name,input){
    var story = getStory(name);
    var defaultWords = getDefaultWords();
    var inputWords = stringObjToArrObj(input);
    var words = combineWords(inputWords,defaultWords);
    story = injectWords(story,words);
    return story;
}

function getStory(name){
    return _stories[name || 'shopping'];
}

function getStories(){
    return Object.keys(_stories);
}

function getDefaultWords(){
    return {
        nouns: _nouns,
        verbs: _verbs,
        adjectives: _adjectives,
        adverbs: _adverbs
    };
}
function getVerbs() {
    return ["leak", "capture", "create", "arrange"];
}
function getNouns() {
    return ["robot", "salamander", "communist", "bounty hunter"];
}
function getAdverbs() {
    return ["weakly", "mischievously", "indecisively", "heavenly"];
}
function getAdjectives() {
    return ["purple", "vengeful", "flirtatious", "definitive"];
}
function getStoriesFromDb() {
    return {
        christmas:"Every Christmas we #verbs to a #adjectives tree farm far away. This is not just any #adjectives tree farm. My dad and I #verbs onto the #nouns to #verbs for the perfect #nouns. Some people like them #adjectives, but I prefer them #adjectives. After searching for hours I usually #adverbs exclaim \"Dad! The perfect tree is over there!\" Off we #verbs to get the tree. The problem is we always forget the #nouns and the #nouns. But at the end of the day we #adverbs get the tree and head home #adverbs. \"I wish it was Christmas all year round\"  I #adverbs think to myself.",
        cowboy:"There once was a #adjectives cowboy who liked to #verbs. Anytime he did, all his friends would #verbs #nouns #adverbs. Once, while rustling #adjectives cattle, he was #adverbs attacked by a #nouns. Using his trusty #nouns, he was able to #adverbs #verbs his #adjectives attackers. It would have been #adjectives if he had decided to #adverbs #verbs his #nouns instead.",
        shopping:"Today I went shopping. When I arrived at the store I saw a #adjectives #nouns, who upon noticing me #adverbs said \"I need to #verbs\". \"Well, that was #adjectives\", I thought to myself before walking to the store. The store had rearranged its inventory, so I felt #adverbs lost. I walked up to store clerk and #adverbs said, \"I am looking for a #adjectives #nouns that doesn’t #verbs as often as the last one I had.\" The store clerk looked at me with a #adjectives look in his eye and said, \"What you are looking for can be found by the #nouns, if you see a #nouns that #adverbs can #verbs, then you have gone too far.\" As I tried to understand his directions, I thought to myself, \"I should have just ordered it on amazon.com, Their products seem to #verbs the perfect amount\""
    }
}